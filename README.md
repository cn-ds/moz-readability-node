# Moz-Readability-Node

Use readability from Mozilla in node software.

[Here is the Mozilla readability repo](https://github.com/mozilla/readability)

[Here is the package I used to build moz-readability-node](https://www.npmjs.com/package/readability-node). The 2 lines of code are exactly the same but with the Readability.js files updated to use the last version of Mozilla. [This commit](https://github.com/mozilla/readability/commit/8fec62d246f563090f5711c02907e2bea7ce96f8)
## What is Readability.js

A standalone version of the readability library used for Firefox Reader View. Any changes to Readability.js itself should be reviewed by an appropriate Firefox/toolkit peer, such as [@leibovic](https://github.com/leibovic) or [@thebnich](https://github.com/thebnich), since these changes will be automatically merged to mozilla-central.

## Usage

To parse a document, you must create a new `Readability` object from a document object, and then call `parse()`. Here's an example:

```javascript
var article = new Readability(document).parse();
```

This `article` object will contain the following properties:

* `title`: article title
* `content`: HTML string of processed article content
* `length`: length of article, in characters
* `excerpt`: article description, or short excerpt from content
* `byline`: author metadata
* `dir`: content direction

If you're using Readability on the web, you will likely be able to use a `document` reference from elsewhere (e.g. fetched via XMLHttpRequest, in a same-origin `<iframe>` you have access to, etc.). Otherwise, you would need to construct such an object using a DOM parser such as [jsdom](https://github.com/tmpvar/jsdom). While this repository contains a parser of its own (`JSDOMParser`), that is restricted to reading XML-compatible markup and therefore we do not recommend it for general use.

### Optional

Readability's `parse()` works by modifying the DOM. This removes some elements in the web page. You could avoid this by passing the clone of the `document` object while creating a `Readability` object.


```
var documentClone = document.cloneNode(true); 
var article = new Readability(documentClone).parse();
```

## License

Copyright (c) 2010 Arc90 Inc

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
